const Movie = require('../models').Movie;
const path = require('path');
const child_process = require('child_process');
const ffmpeg = require('fluent-ffmpeg');
const encod = require('../encoding');

// Retrieve all movies
module.exports.getAll = async (req, res, next) => {
    // We should query the database
    try {
        const movies = await Movie.findAll();
        res.status(200).json(movies);
    }
    catch (error) {
        return next(error);
    }
};

// Create a new movie
module.exports.create = async (req, res, next) => {
    // No validation needed
    try {
        const movie = await Movie.create( { title: req.body.title, author: req.body.author } );
        res.status(201).json(movie);
    }
    catch (error) {
        return next(error);
    }
};

// Get an existing movie
module.exports.get = async (req, res, next) => {
    // No validation needed
    try {
        const movie = await Movie.findByPk( req.params.id );
        if (movie) {
            res.status(200).json(movie);
        }
        else {
            res.status(404).end();
        }
    }
    catch (error) {
        return next(error);
    }
};

// Upload film image to an existing movie
module.exports.upload = async (req, res, next) => {
    try {
        const movie = await Movie.findByPk( req.params.id );
        if (!movie) {
            res.status(404).end();
            return;
        }

        // A�adimos el video a su titulo
        const filmFile = req.files.filmFile;
        const extension = path.extname(filmFile.name);       
        const destination = '/film/film-' + movie.id + extension;
        filmFile.mv(destination);

        const destination2 = '/capturas/film-' + movie.id + '.png';
        
        if(extension == ".mp4"){

        }else{
            encod.encodeMp4(destination);
        }

        const destination3 = '/film/film-' + movie.id + '.mp4';

        //const mpd = '/film/film-' + movie.id + '.mpd' + '/stream.mpd';
        
        encod.getThumbnail(destination3);

        encod.encodeDash(destination3);

         // Update movie
        await movie.update({ capturas: destination2, film: destination3 });
        res.status(200).json(movie);

    }
    catch (error) {
        return next(error);
    }
};