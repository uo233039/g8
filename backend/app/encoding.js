const path = require('path');
const child_process = require('child_process');


const formats = [
    ".mp4",
    ".mov",
    ".avi",
    ".wmv",
    ".mkv"
];


exports.encodeMp4 = (filmFile) => {
    return new Promise((resolve, reject) => {
        const parsedFile = path.parse(filmFile);
        const outputFile = `${parsedFile.name}.mp4`
        // /videos is the shared volume with nginx http server
        const outputFilePath = `/film/${outputFile}`;
        // ffmpeg command to convert the video to mp4 format with stardard codec
        const command = `ffmpeg -y -i ${filmFile} ${outputFilePath}`;

        // Is it the right extension?    
        if (!formats.includes(parsedFile.ext)) {
        return reject(new Error("wrong video format" ));
        }

        // Encode
        child_process.exec(command, (err, stdout, stderr) => {
        if (err) {
            return reject(new Error(`Encoding error. ${stderr}`));
        }
        resolve(outputFilePath);
        });
    });
}


exports.getThumbnail = (filmFile) => {
    return new Promise((resolve, reject) => {
      const parsedFile = path.parse(filmFile);
      const outputFile = `${parsedFile.name}.png`
      // /videos is the shared volume with nginx http server
      const outputFilePath2 = `/capturas/${outputFile}`;
      // ffmpeg command to extract a frame at the beginning of video and copy it to ${outputFilePath} 
      const command = `ffmpeg -i ${filmFile} -ss 00:00:01 -vframes 1  ${outputFilePath2}`;

      // Encode
      child_process.exec(command, (err, stdout, stderr) => {
        if (err) {
          return reject(new Error(`Encoding error. ${stderr}`));
        }
        resolve(outputFilePath2);
      });
    });
}

exports.encodeDash = (filmFile) => {
    return new Promise((resolve, reject) => {
      const parsedFile = path.parse(filmFile);
      const outputFile = `${parsedFile.name}.mpd`
      // /Movies is the shared volume with nginx http server
      const outputFilePath = `/film/${outputFile}`;
      // command to run a bash script for dash encoding
      const command = `ffmpeg -i ${filmFile} -c:v libx264 -r 24 -g 24 -b:v 1000k -maxrate 1000k -bufsize 2000k -strict -2 /film/1000k_${parsedFile.name}.mp4 &&
      ffmpeg -i ${filmFile} -c:v libx264 -r 24 -g 24 -b:v 500k -maxrate 500k -bufsize 1000k -strict -2 /film/500k_${parsedFile.name}.mp4 &&
      ffmpeg -i ${filmFile} -c:v libx264 -r 24 -g 24 -b:v 250k -maxrate 250k -bufsize 500k -strict -2 /film/250k_${parsedFile.name}.mp4 &&
      bento4/bin/mp4fragment --fragment-duration 2000 /film/1000k_${parsedFile.name}.mp4 /film/frag_1000k_${parsedFile.name}.mp4 &&
      bento4/bin/mp4fragment --fragment-duration 2000 /film/500k_${parsedFile.name}.mp4 /film/frag_500k_${parsedFile.name}.mp4 &&
      bento4/bin/mp4fragment --fragment-duration 2000 /film/250k_${parsedFile.name}.mp4 /film/frag_250k_${parsedFile.name}.mp4 &&
      python bento4/utils/mp4-dash.py --use-segment-timeline -o /film/sintel-${parsedFile.name}  /film/frag_1000k_${parsedFile.name}.mp4 /film/frag_500k_${parsedFile.name}.mp4 /film/frag_250k_${parsedFile.name}.mp4`;


      
      // Encode
      child_process.exec(command, (err, stdout, stderr) => {
        if (err) {
          return reject(new Error(`Encoding error. ${stderr}`));
        }
        resolve(outputFilePath);
      });

	console.log("LlegueAqui")

    });
}
