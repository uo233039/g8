module.exports = app => {
    const controller = require("../controllers/movie.controller");
    const validator = require('../middlewares/validators/movie.validator');
    const baseRoute = '/movie'

    app.get(baseRoute, controller.getAll);

    app.post(baseRoute, validator.create, controller.create);

    app.get(`${baseRoute}/:id`, controller.get);

    app.post(`${baseRoute}/:id/upload`, validator.upload, controller.upload)
};