module.exports = (sequelize, Sequelize) => {
    const Movie = sequelize.define("movies", {  // Table name and fields
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        title: {
            type: Sequelize.STRING,
            allowNull: false
        },
        author: {
            type: Sequelize.STRING,
            allowNull: false
        },
        film: {
            type: Sequelize.STRING,
            allowNull: true,
            defaultValue: '/film/default.mp4',
        },    
        capturas: {
            type: Sequelize.STRING,
            allowNull: true,
            defaultValue: '/capturas/default.png'
        
        },
    });

    return Movie;
};
