const bcrypt = require('bcrypt');
const config = require('../../app/config/auth.config');
const app = require('../../app/app');
const chai = require('chai');
const expect = require('chai').expect;
const db = require('../../app/models');
const Movie = db.Movie;
const User = db.User;

module.exports.populateMovies = async () => {
    await Movie.create( { title: 'El guardián entre el centeno', author: 'J.D. Salinger' } );
    await Movie.create( { title: 'La vida es sueño', author: 'Calderón de la Barca' } );
}

module.exports.dropMovies = async () => {
    await Movie.destroy({  truncate: true, force: true, restartIdentity: true });
}

module.exports.populateUsers = async () => {
    await User.create( { username: 'Username1', password: bcrypt.hashSync('Password1', config.salt) } );
    await User.create( { username: 'Username2', password: bcrypt.hashSync('Password2', config.salt) } );
}

module.exports.dropUsers = async () => {
    await User.destroy({  truncate: true, force: true, restartIdentity: true });
}

module.exports.login = (username, password) => {
    return new Promise((resolve, reject) => {
        chai.request(app)
            .post('/login')
            .send({ username: username, password: password })
            .end((err, res) => {
                expect(res).to.have.status(200);
                expect(res).to.be.json;
                expect(res.body).to.have.property('token');                    
                resolve(res.body.token);
            });
        });
}