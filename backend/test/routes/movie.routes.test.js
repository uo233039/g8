const app = require('../../app/app');
const chai = require('chai');
const expect = require('chai').expect;
const utils = require('./utils');
const fs = require('fs');

chai.use(require('chai-http'));
chai.use(require('chai-arrays'));

const MOVIE_URI = '/movie';


describe('Get movies: ', () => {

    /**
     * Populate database with some data before all the tests in this suite.
     */
    before(async () => {        
        await utils.populateMovies();
    });

    /**
     * This is run once after all the tests.
     */
    after(async () => {
        await utils.dropMovies();
    });

    /**
     * Get all movies correctly
     */
    it('should get all movies', (done) => {
        chai.request(app)
            .get(MOVIE_URI)
            .end((err, res) => {
                expect(res).to.have.status(200);
                expect(res).to.be.json;
                expect(res.body).to.be.array();
                expect(res.body.length).to.equal(2);
                done();
            });
    });

    /**
     * Get an existing movie correctly
     */
    it('should get a movie', (done) => {
        chai.request(app)
            .get(MOVIE_URI + '/2')
            .end((err, res) => {
                expect(res).to.have.status(200);
                expect(res).to.be.json;
                expect(res.body.id).to.be.equal(2);
                expect(res.body.title).to.be.equal('La vida es sueño');
                expect(res.body.author).to.be.equal('Calderón de la Barca');
                done();
            });
    });
});


describe('Create movies: ', () => {

    /**
     * Populate database with some data before all the tests in this suite.
     */
    before(async () => {        
        await utils.populateMovies();
        await utils.populateUsers();
        token = await utils.login('Username1', 'Password1');
    });

    /**
     * This is run once after all the tests.
     */
    after(async () => {
        await utils.dropMovies();
        await utils.dropUsers();
    });

    /**
     * A movie should be created correctly
     */
    it('should create a valid movie', (done) => {
        const title = "Harry Potter y la piedra filosofal";
        const author = "J.K. Rowling";
        chai.request(app)
            .post(MOVIE_URI)
            .set('Authorization', 'Bearer ' + token)
            .send({ title: title, author: author })
            .end((err, res) => {
                expect(res).to.have.status(201);
                expect(res).to.be.json;
                expect(res.body.title).to.be.equal(title);
                expect(res.body.author).to.be.equal(author);
                expect(res.body.id).to.be.equal(3);
                done();
            });
    });

    /**
     * An invalid title should raise an error
     */
    it('should receive an error with an invalid title', (done) => {
        chai.request(app)
            .post(MOVIE_URI)
            .set('Authorization', 'Bearer ' + token)
            .send({ title:"", author: "Unknown" })
            .end((err, res) => {
                expect(res).to.have.status(422);
                done();
            });
    });

    /**
     * A missing title should raise an error
     */
    it('should receive an error with missing name', (done) => {
        chai.request(app)
            .post(MOVIE_URI)
            .set('Authorization', 'Bearer ' + token)
            .send({ author: "Unknown" })
            .end((err, res) => {
                expect(res).to.have.status(422);
                done();
            });
    });

    /**
     * An invalid author should raise an error
     */
    it('should receive an error with an invalid author', (done) => {
        chai.request(app)
            .post(MOVIE_URI)
            .set('Authorization', 'Bearer ' + token)
            .send({ title:"A valid title", author: "No" })
            .end((err, res) => {
                expect(res).to.have.status(422);
                done();
            });
    });

    /**
     * A missing author should raise an error
     */
    it('should receive an error with missing author', (done) => {
        chai.request(app)
            .post(MOVIE_URI)
            .set('Authorization', 'Bearer ' + token)
            .send({ title: "A valid title" })
            .end((err, res) => {
                expect(res).to.have.status(422);
                done();
            });
    });
});

describe('Upload movie films: ', () => {

    /**
     * Populate database with some data before all the tests in this suite.
     */
    before(async () => {        
        await utils.populateMovies();
        await utils.populateUsers();
        token = await utils.login('Username1', 'Password1');
    });

    /**
     * This is run once after all the tests.
     */
    after(async () => {
        await utils.dropMovies();
        await utils.dropUsers();
    });

    /**
     * A movie film should be uploaded correctly
     */
    it('should upload a movie film', (done) => {
        chai.request(app)
            .post(MOVIE_URI + '/2/upload')
            .set('Authorization', 'Bearer ' + token)
            .attach('filmFile', fs.readFileSync('./test/assets/film.mp4'), 'film.mp4')
            .end((err, res) => {
                expect(res).to.have.status(200);
                expect(res).to.be.json;
                expect(res.body.id).to.be.equal(2);
                expect(res.body.title).to.be.equal('La vida es sueño');
                expect(res.body.author).to.be.equal('Calderón de la Barca');
                expect(res.body.film).to.be.equal('/film/film-2.mp4');
                done();
            });
    }).timeout(5000);  // Timeout 5 secs


    /**
     * TODO: tests for invalid upoloads
     */
});
